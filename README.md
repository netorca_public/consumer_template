# Consumer Template Repository

This repository can be cloned directly to customise and create a Consumer repository for use with NetOrca. 

## CI/CD

This repository has a CI/CD configuration for gitlab that will:
* Validate the configuration on any merge to master
* Submit the new configuration once the repository has been merged to master. 



